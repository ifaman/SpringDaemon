package com.ifaman.daemon;

import static org.mockito.Mockito.*;

import org.junit.Test;

public class KoreanTest {

	@Test
	public void test() {
		IMessage mock = mock(IMessage.class);
		
		Korean korean = new Korean(mock);
		korean.sayhello();
		korean.sayhello();
		
		verify(mock, times(2)).hello();
	}

}
