package com.ifaman.daemon;

public class Korean implements IPerson {
	private IMessage msg;
	
	public Korean(IMessage hello){
		this.msg = hello;
	}
	
	public void sayhello() {		
		msg.hello();
	}

}
