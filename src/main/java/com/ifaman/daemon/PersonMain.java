package com.ifaman.daemon;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersonMain {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("hello.xml");
		IPerson person = (IPerson)context.getBean("korea");
		
		person.sayhello();
	}
}
